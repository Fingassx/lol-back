const express = require('express');
const path = require('path');
const serveStatic = require('serve-static');
//const mongoose = require("mongoose");
const cors = require('express-cors')

const champions = require('./routes/champions');
const summoners = require('./routes/summoners');

//const connectionString = require('./connections/connections');

const app = express();

//mongoose.connect(connectionString);

//mongoose.connection.on("error", console.error.bind(console, "Mongo connection error:"));
/*mongoose.connection.once("open", () => {
    console.log("Connected to Mongo");
    global.gfs = Grid(mongoose.connection.db, mongoose.mongo);
    global.gfsf = GridFile(mongoose.connection.db, mongoose.mongo)
  });*/

app.use(cors({
    allowedOrigins: ['localhost:3000']
}));

app.use(serveStatic(path.join(__dirname, 'public')));
app.use('/champions', champions);
app.use('/summoners', summoners);

app.listen(8000);