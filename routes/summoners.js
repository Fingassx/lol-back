const summoners = require('express').Router();
const fetch = require('node-fetch');

const key = 'RGAPI-d521c278-bdc0-464f-96fc-34c87165bd2b';
const summonerPath = (name) => 'https://eun1.api.riotgames.com/lol/summoner/v3/summoners/by-name/' + name;
const recentPath = (id) => 'https://eun1.api.riotgames.com/lol/match/v3/matchlists/by-account/' + id ;
const headers = {
    "Origin": null,
    "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
    "X-Riot-Token": key,
    "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3"
}

const summonersStatic = {}

const getMatchHistory = (id) =>{
    let matchHistory = {};
    fetch( recentPath(id) ,{method:'GET', headers:headers})
    .then(response=>response.json())
    .then(json=> {
        matchHistory = json;
    });
    return matchHistory;
}

summoners.get('/:name', (req, res)=>{
    res.contentType('application/json');
    if(summonersStatic[req.params.name+'']){
        res.send(summonersStatic[req.params.name+'']);
        console.log('Summoner sended');
    }
    else{
    fetch( summonerPath(req.params.name) ,{method:'GET', headers:headers})
    .then(response=>response.json())
    .then(json=> {
        console.log(json)
        if(json.name){
        summonersStatic[req.params.name+''] = json;
        summonersStatic[req.params.name+''].recentMatchHistory = getMatchHistory(summonersStatic[req.params.name+''].accounId);
        console.log(json);
        res.send(summonersStatic[req.params.name+'']);
        }else{
            res.send({error:'invalid name'});
        }
    });
}
})

module.exports = summoners;