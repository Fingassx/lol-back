const champions = require('express').Router();
const fetch = require('node-fetch');
             
const key = 'RGAPI-d521c278-bdc0-464f-96fc-34c87165bd2b';
const path = 'https://eun1.api.riotgames.com/lol/static-data/v3/champions?locale=en_US&champListData=image&dataById=true&api_key=' + key;
const champPath = (id) => 'https://eun1.api.riotgames.com/lol/static-data/v3/champions/' + id + '?locale=en_US&champData=all&api_key=' + key;
const headers = {
    "Origin": null,
    "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
    "X-Riot-Token": key,
    "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3"
}

const championsStatic = {};
const championStatic = [];

champions.get('/', (req, res)=>{
    res.contentType('application/json');
    if(championsStatic.listChamps){
        res.send(championsStatic.listChamps);
        console.log('List sended');
    }
    else{
    fetch( path ,{method:'GET', headers:headers})
    .then(response=>response.json())
    .then(json=> {
        championsStatic.listChamps = json;
        console.log(json);
        res.send(json);
    });
}
})

champions.get('/:id', (req, res)=>{
    res.contentType('application/json');
    if(championStatic[req.params.id]){
        res.send(championStatic[req.params.id]);
        console.log('Champ sended');
    }
    else{
    fetch( champPath(req.params.id) ,{method:'GET', headers:headers})
    .then(response=>response.json())
    .then(json=> {
        championStatic[req.params.id] = json;
        console.log(json);
        res.send(json);
    });
}
})

module.exports = champions